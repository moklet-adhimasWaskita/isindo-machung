const express = require('express')
const bp = require('body-parser')
const mongoose = require('mongoose')
const app = express();
const cors = require('cors');

const malangKotaWisata = require('./malang-kota-wisata');
const malangKotaKuliner = require('./malang-kota-kuliner');
const batuKotaWisata = require('./batu-kota-wisata');
const batuKotaKuliner = require('./batu-kota-kuliner');
const malangKabWisata = require('./malang-kab-wisata')

mongoose.connect('mongodb://aang:aangaang1@ds121455.mlab.com:21455/isindo');
mongoose.Promise = global.Promise;

app.use(bp.json());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
  });

app.use('/malangKotaWisata', malangKotaWisata);
app.use('/malangKotaKuliner', malangKotaKuliner);
app.use('/batuKotaWisata', batuKotaWisata);
app.use('/batuKotaKuliner', batuKotaKuliner);
app.use('/malangKabWisata', malangKabWisata);

//error middleware
app.use((err,req,res,next)=>{
    res.status(422).json({err: err.message});
})

app.listen(5000, function(){
    console.log('express app now listening request at port : 5000');
})