const express = require('express');
const wisataKotaMalang = express.Router();
const model = require('./models/modelKotaMalangWisata')

wisataKotaMalang.get('/list', function(req, res, next) {
    try {
        model.find((err,result)=>{
            if(result.length > 0 ){
                res.status(200).json({
                    code : 200,
                    message : "Succes",
                    data : result
                })
            }
            else if (result.length == 0) {
                res.status(404).json({
                    code : 404,
                    message : "Data not Found",
                })
            }
            else if (err) {
                res.json({
                    message : err
                })
            }
        })
    } catch (error) {
        next(error)
    }
})

wisataKotaMalang.post('/find', (req, res, next)=>{

    model.find(model._id = req.body,(error, result)=>{
        try {
            if(error) {
                throw error
            }
            else {
                res.status(200).json({
                    code : 200,
                    message : "Berhasil get data",
                    data : result
                })
            }
        } catch(err) {
            next (err)
        }
    })
})

module.exports = wisataKotaMalang;