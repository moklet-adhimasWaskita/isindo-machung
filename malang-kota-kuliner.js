const express = require('express');
const kulinerKotaMalang = express.Router();
const model = require('./models/modelKotaMalangKuliner')


kulinerKotaMalang.get('/list', function(req, res, next) {
    try {
        model.find((err,result)=>{
            if(result.length > 0) {
                res.status(200).json({
                    code : 200,
                    message : "Succes",
                    data : result
                })
            }
            else if (result.length == 0) {
                res.status(404).json({
                    code : 404,
                    message : "Data not Found",
                })
            }
            else if (err) {
                res.json({
                    message : err
                })
            }
        })
    } catch (error) {
        next(error)
    }
})



module.exports = kulinerKotaMalang;