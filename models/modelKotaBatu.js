const mongoose = require('mongoose');
const schema = mongoose.Schema;

const kotaBatuWisataSchema = new schema({
    nama_destinasi : {
        type : String,
        required : true
    },
    alamat : {
        type : String,
        required : true
    },
    harga_tiket : {
        type : Number 
    },
    jam_buka : {
        type : String,
        required : false,
    },
    foto : {
        type : String
    },
    kategori : {
        type : String 
    }
})

const kotabatuWisata = mongoose.model('kotabatudestinations', kotaBatuWisataSchema);

module.exports = kotabatuWisata;