const mongoose = require('mongoose');
const schema = mongoose.Schema;

const kabBatuWisataSchema = new schema({
    nama_destinasi : {
        type : String,
        required : true
    },
    alamat : {
        type : String,
        required : true
    },
    harga_tiket : {
        type : Number 
    },
    jam_buka : {
        type : String,
        required : false,
    },
    foto : {
        type : String
    }
})

const kabMalangWisata = mongoose.model('kabmalangdestinations', kabBatuWisataSchema);

module.exports = kabMalangWisata;