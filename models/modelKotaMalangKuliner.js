const mongoose = require('mongoose');
const schema = mongoose.Schema;

const kabMalangKulinerSchema = new schema({
    nama_destinasi : {
        type : String,
        required : true
    },
    alamat : {
        type : String,
        required : true
    },
    jam_buka : {
        type : String,
        required : false,
    },
    harga : {
        type : String,
        required : true
    },
    foto : {
        type : String
    }
})

const kabMalangKuliner = mongoose.model('kotamalangkuliner', kabMalangKulinerSchema);

module.exports = kabMalangKuliner;