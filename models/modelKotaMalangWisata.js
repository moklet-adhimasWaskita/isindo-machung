const mongoose = require('mongoose');
const schema = mongoose.Schema;

const kotaMalangWisataSchema = new schema({
    nama_destinasi : {
        type : String,
        required : true
    },
    alamat : {
        type : String,
        required : true
    },
    jam_buka : {
        type : String,
        required : false,
    },
    harga_tiket : {
        type : Number 
    },
    foto : {
        type : String
    }
})

const kotaMalangWisata = mongoose.model('kotaMalangDestinations', kotaMalangWisataSchema);

module.exports = kotaMalangWisata;